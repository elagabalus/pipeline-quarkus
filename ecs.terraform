terraform {
	required_providers {
		aws = {
			source  = "hashicorp/aws"
			version = "3.54.0"
		}
	}
}

// obtener datos de la cuenta
data "aws_caller_identity" "current" {}

#locals { 
#	tf_account_id = data.aws_caller_identity.current.account_id
#}

provider "aws" {
	region	= "us-west-2"
	profile	= "cicd-neto-dev"
}



# Crear Log Group para CloudWatch
resource "aws_cloudwatch_log_group" "LG_TD_ECS" {
	name = "REPLACE_LOG_GROUP_CLOUDWATCH_NAME"
	tags = {
		Name        = "REPLACE_CLOUDWATCH_NAME"
	}
}

# Crear Task Definition
resource "aws_ecs_task_definition" "TD_ECS" {
	family                   = "REPLACE_TASK_DEFINITION_FAMILY"
	network_mode             = "awsvpc"
	requires_compatibilities = ["FARGATE"]
	cpu                      = REPLACE_CPU
	memory                   = REPLACE_MEMORY
	execution_role_arn       = "REPLACE_EXECUTION_ROLE"
	task_role_arn            = "REPLACE_TASK_ROLE"

	container_definitions = jsonencode([{
		name        = "REPLACE_CONTAINER_NAME"
		image 		= "REPLACE_DOCKER_IMAGE_TAG"
		essential   = true
		portMappings = [{
			protocol      = "tcp"
			containerPort = REPLACE_APLICATION_PORT
			hostPort      = REPLACE_APLICATION_PORT
		}]
		
		logConfiguration = {
			logDriver = "awslogs"
			options = {
				awslogs-group         = aws_cloudwatch_log_group.LG_TD_ECS.name
				awslogs-stream-prefix = "ecs"
				awslogs-region        = "REPLACE_REGION"
			}
		}
	 }])
}

# Crear Target Group para ECS
resource "aws_alb_target_group" "TG_SERVICE" {
	name        = "REPLACE_TG_NAME"
	port        = 80
	protocol    = "HTTP"
	vpc_id      = "REPLACE_VPC_ID"
	target_type = "ip"

	health_check {
	 healthy_threshold   = "3"
	 interval            = "40"
	 protocol            = "HTTP"
	 matcher             = "200"
	 timeout             = "10"
	 path                = "REPLACE_HEALT_CHECK_PATH"
	 unhealthy_threshold = "2"
	}
}

# Crear Service
resource "aws_ecs_service" "SERVICE_ECS" {
	name                               = "REPLACE_SERVICE_NAME"
	cluster 						   = "REPLACE_CLUSTER_NAME"
	task_definition                    = aws_ecs_task_definition.TD_ECS.arn
	desired_count                      = 1
	deployment_minimum_healthy_percent = 100
	deployment_maximum_percent         = 200
	launch_type                        = "FARGATE"
	scheduling_strategy                = "REPLICA"

	network_configuration {
		#security_groups 	 = [aws_security_group.SG_SERVICE.id]
		security_groups 	 = REPLACE_SECURITY_GROUP_NAME
		subnets 			 = REPLACE_SUBNETS
		assign_public_ip 	 = false
	}

	load_balancer {
		target_group_arn = aws_alb_target_group.TG_SERVICE.arn
		container_name   = "REPLACE_CONTAINER_NAME"
		container_port   = "REPLACE_APLICATION_PORT"
	}
}

resource "aws_lb_listener_rule" "ALB_STATIC_RULE" {
  listener_arn 			 = "REPLACE_LISTENER_ARN"
  priority     			 = "REPLACE_LISTENER_PRIORITY"
  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.TG_SERVICE.id
  }
  condition {
    path_pattern {
      values = ["REPLACE_BASE_PATH"]
    }
  }
}
