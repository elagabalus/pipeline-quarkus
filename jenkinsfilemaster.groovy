import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput
import java.time.LocalDateTime
// sudo update-alternatives --config 'java'
@NonCPS

//PREFIJO = "cicd-"
//SUBFIJO = "-dev"
//isBig
// # 256 (.25 vCPU) - Available memory values: 512 (0.5 GB), 1024 (1 GB), 2048 (2 GB) -> TINY
// # 512 (.5 vCPU) - Available memory values: 1024 (1 GB), 2048 (2 GB), 3072 (3 GB), 4096 (4 GB) ->
// # 1024 (1 vCPU) - Available memory values: 2048 (2 GB), 3072 (3 GB), 4096 (4 GB), 5120 (5 GB), 6144 (6 GB), 7168 (7 GB), 8192 (8 GB)
// # 2048 (2 vCPU) - Available memory values: Between 4096 (4 GB) and 16384 (16 GB) in increments of 1024 (1 GB)
// # 4096 (4 vCPU) - Available memory values: Between 8192 (8 GB) and 30720 (30 GB) in increments of 1024 (1 GB)
// Cpu: !If [isTiny,256,!If [isSmall,512,!If [isMedium,1024,!If [isBig,2048,2048]]]]
// Memory: !If [isTiny,512,!If [isSmall,1024,!If [isMedium,2048,!If [isBig,4096,8192]]]]

// [cicd-neto-dev]
// aws_access_key_id = AKIAS7EWWIM6PJJEB36R
// aws_secret_access_key = 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


TELEGRAM_GROUP = "-873535927" //"-897655700"//<----- Despliegues Neto"
CI_SERVER_URL = "http://44.197.211.53"
deployInfo = [:]

def mainFlow() { 
    try{
        cloningMicroservice()
        initVars()
        print "if validationECR"
        if(validationECR(deployInfo)){
            print "validationECR"
            addExtraParams(deployInfo)//<-----------borrar
            buildImage(deployInfo)
            if(pushImageECR(deployInfo)){
                //paramsEnvFile()
                def existService = isExistService(deployInfo)
                dir("quarkus-deploy-pipeline"){
                    if (!existService){
                        print "No Existe el servicio ${deployInfo.serviceName}"                
                        createAllECS()
                    }else{
                        print "Existe el servicio: ${deployInfo.serviceName}"
                        newTaskRevision()
                        updateService(deployInfo)
                    }
                }
            }else{
                currentBuild.result = 'FAILED'
            }
            purgaImagenes(deployInfo)
            echo "Despliegue finalizado del proyecto ${deployInfo.serviceName }"
        }else{
            print "Error no se pudo crear el ECR"
            currentBuild.result = 'FAILED'
            return
        }
    }catch(err){
        currentBuild.result = 'FAILED'
        throw err
    }finally{
        echo "finally"
        //cleanWorkSpace()
        //STG-NOTIFY
        //print "${currentBuild.getBuildCauses()[0]}"
        //echo sh(script: 'env|sort', returnStdout: true);
        stage("Send Deploy Notifications"){
            //print '--------------------->stage(Send Deploy Notifications)<----------------------------'
            currentBuild.result = currentBuild.result.equals('FAILED') ? 'FAILED' : 'SUCCESS'
            //sendNotifications(currentBuild.result)
        }
    }

}


//-----------------------Inicializa Variables

def initVars(){
    stage("initVars"){
        println "------------------> initVars() <------------------"
        //println "datosRepo: "+datosRepo
        def cicdInfo = [:]
        def projectName = datosRepo.name
        def splitName = projectName.split("-") 
        def shortType = ""
        print "bandera 1"
        sh "cp ${projectName}/params.tfvars.json ./quarkus-deploy-pipeline/"
        def jsonParams = readJSON file: './quarkus-deploy-pipeline/params.tfvars.json'
        splitName = jsonParams.tf_service_type
        dir("quarkus-deploy-pipeline") {
            // if(splitName[0].equals("microservices")){
            //     splitName = "microservice"
            // }else{
            //     splitName = "${splitName[0]}-${splitName[1]}"
            // }            
            print "bandera 2"
            switch(splitName) {
                case "microservice":
                    shortType = "MS"
                break
                case "entity-service":
                    shortType = "ES"
                break
                 case "utility-service":
                    shortType = "US"
                break
                case "task-service":
                    shortType = "TS"
                break
                default:
                    shortType = "NA"
                break
            }           
        }
        
        print "bandera 3"
        cicdInfo.projectName =  projectName
        cicdInfo.shortName = jsonParams.tf_proyect_name 
        cicdInfo.shortType = shortType
        print "bandera 4"
        deployInfo.jfrogpass = "d3vs3c0ps"
        deployInfo.jfrogurl = "http://35.153.28.141:8081/artifactory/neto-microservices-gradle-release"
        deployInfo.jfroguser = "admin"
        deployInfo.jfrogrepokey = "neto-microservices-gradle-release-local"
        deployInfo.jfrogcontexturl ="http://35.153.28.141:8081/artifactory"
        CORREO = "angel.ivan.guadarrama@gmail.com"
        print "bandera 5"
        deployInfo.awsProfile = "--profile cicd-neto-dev"
        //deployInfo.awsProfile = ""
        deployInfo.accountId = getAccountData(deployInfo)
        deployInfo.projectName = cicdInfo.projectName        
        deployInfo.tagVersion = ""
        deployInfo.area = ""
        deployInfo.ecrName = "${cicdInfo.projectName}"
        deployInfo.serviceName = "SRV-${cicdInfo.projectName.toUpperCase()}"
        deployInfo.dockerImageTag = ""
        deployInfo.targetGroup = "TG-${cicdInfo.shortType}-${cicdInfo.shortName.toUpperCase()}"
        deployInfo.logGroup = cicdInfo.projectName
        deployInfo.cloudWatch = "cloudwatch-${cicdInfo.projectName}"
        deployInfo.taskDefinition = "TD-${cicdInfo.projectName.toUpperCase()}"
        deployInfo.container = "container-${cicdInfo.projectName}"
        deployInfo.listenerArn = ""
        deployInfo.listenerPriority = ""
        deployInfo.basePath = jsonParams.base_path
        deployInfo.healthCheckPath = jsonParams.health_check_path
        deployInfo.cpu = jsonParams.tf_cpu
        deployInfo.memory = jsonParams.tf_memory
        deployInfo.taskRole = "arn:aws:iam::${deployInfo.accountId}:role/ecsTaskExecutionRole"
        deployInfo.executionRole = "arn:aws:iam::${deployInfo.accountId}:role/ecsTaskExecutionRole"
        deployInfo.aplicationPort = jsonParams.tf_application_port
        deployInfo.subnets = '["subnet-0b0332a150aa6f2d4", "subnet-0c15694accd3ab017"]'
        deployInfo.vpc = "vpc-0105becbf877eef95"
        deployInfo.securityGroup = '["sg-0307ae23522bee9de"]'
        deployInfo.cluster = "CLUSTER-NETO-MS"
        deployInfo.envFile = "${BRANCH_NAME}.env"
        deployInfo.region = "us-west-2"
        deployInfo.loadbalancer = "ALB-PUBLICO-MS-NETO"
        deployInfo.ecrArn = "${deployInfo.accountId}.dkr.ecr.${deployInfo.region}.amazonaws.com"
        print "bandera 6"
    }    
}
def getAccountData(def data){
        def awsProfile = data.awsProfile
            def accountData = sh(script: "aws ${awsProfile} sts get-caller-identity", returnStdout: true)
            accountData = jsonParse(accountData)
            return accountData["Account"]
        }
//--------------------Termina inicializar variables

// -------------------------- UTILS -------------------------
def paramsEnvFile(){
     // construyendo archivo de variables de entorno para parameters store
    echo 'paramsEnvFile()'
    dir("${FULL_NAME}"){
        print "params.env_file_name: ${ENV_FILE_NAME}"
        if(ENV_FILE_NAME) {
            buildEnvToContainerTD(ENV_FILE_NAME, REGION, ACCOUNT_ID) }
    }
}

def readLocalFile(fileName) {
    def file = null
    file = readFile file: "${fileName}"
    return file
}

def buildEnvToContainerTD(fileName, region, accountId) {
    echo "buildEnvToContainerTD(${fileName}, ${region}, ${accountId})"
    def environments = readLocalFile(fileName)
    print "environments: ${environments}"
    if(!environments) {
        return null
    }

    def splitedFile = environments.split("\n")
    print "splitedFile: ${splitedFile}"
    def buildedEnvs = []
    print "buildedEnvs: ${buildedEnvs}"   
    splitedFile.each{environment ->
    print "splitedFile.each{${environment} ->"
        buildedEnvs.push([
            "name": "${environment}",
            "valueFrom": "arn:aws:ssm:${region}:${accountId}:parameter/${environment}"
        ])
    }
    print "buildedEnvs: ${buildedEnvs}"
    writeFile file: "env.json", text: String.valueOf(buildedEnvs)
}

def jsonParse(json) {
    return new JsonSlurperClassic().parseText(json)
}

def generateImageVersion() {
    
}

def addExtraParams(def data) {
    def albName = data.loadbalancer
    print "addExtraParams(${albName})"
    def awsProfile = data.awsProfile
    def albRequest = sh(script: "aws ${awsProfile} elbv2 describe-load-balancers --names ${albName}", returnStdout: true)
    def albRequestJson = jsonParse(albRequest)
    def albArn = albRequestJson["LoadBalancers"]
    albArn = albArn[0]
    albArn = albArn["LoadBalancerArn"]    
    def albListenersRequest = sh(script: "aws ${awsProfile} elbv2 describe-listeners --load-balancer-arn ${albArn}", returnStdout: true)
    def albListenersRequestJson = jsonParse(albListenersRequest)
    def albListener = albListenersRequestJson["Listeners"].find { listener -> listener["Port"] == 80 }
    def listener = albListener["ListenerArn"]
    deployInfo.listenerArn = listener
    deployInfo.listenerPriority = identificaReglas(deployInfo)
    print deployInfo.listenerPriority
    print "Termina addExtraParams"
    //print "deployInfo.listenerArn: ${deployInfo.listenerArn}"
    //print "deployInfo.listenerPriority: ${deployInfo.listenerPriority}"
}

def identificaReglas(def data){
    def awsProfile = data.awsProfile
    def listenerArn = data.listenerArn
    //***************************
    def reglas = sh(script: "aws ${awsProfile} elbv2 describe-rules --listener-arn ${listenerArn} | grep 'Priority'", returnStdout: true)
    //print "reglas: ${reglas}"

    //*********************
    def buscaNumeros = (reglas =~ /([0-9])+/)
    def NUM = []
    def contadorMatchers=0
    buscaNumeros.each{match->
        NUM[contadorMatchers] = match[0].toInteger()
        contadorMatchers++
    }
    //print "return ${NUM}"
    reglas = NUM
    //***********************
    //print "reglas.size(): ${reglas.size()}"
    def temp = 0
     if(reglas.size()!=0){
        //echo "if(reglas.size()!=0){"
        //echo "${reglas.size()}!=${reglas[reglas.size()-1]}" 
        //echo "reglas: ${reglas}"
        for(int iteradorReglas=0;iteradorReglas<reglas.size();iteradorReglas++){
            //print "reglas.size(): ${reglas.size()}"
            //print "iteradorReglas: ${iteradorReglas}"
            int salidadeFuncion=0
            //print "reglas[${iteradorReglas}]: ${reglas[iteradorReglas]}"
            int comparador = reglas[iteradorReglas].toInteger()
            //print "comparador: ${comparador}"
            temp = comparador
            //print "temp: ${temp}"
            if(reglas.size()>=1){
                salidadeFuncion = 1
                //print "if(reglas.size()>1){"
                for(int iteradorComparador=0;iteradorComparador<iteradorReglas;iteradorComparador++){
                    //print "for(int ${iteradorComparador}=0;${iteradorComparador}<=${iteradorReglas};${iteradorComparador++}){"
                    //print "iteradorComparador: ${iteradorComparador}"
                    //print "reglas: ${reglas}"
                    //print "reglas[iteradorComparador+1]: ${reglas[iteradorComparador+1]}"
                    int comparadorDos = reglas[iteradorComparador].toInteger()+1
                    //print "comparadorDos: ${comparadorDos}"
                    if((comparadorDos-temp)>=2){
                        println("ultimo numero con secuencia:::::::::::::::::|$temp|::::::::::::::::::::::::se agregara 1 para la prioridad")
                        salidadeFuncion=temp
                        break
                    }
                }
            }
            if(salidadeFuncion!=0){
                //echo "if(salidadeFuncion!=0){"
                temp = temp+1
            }
        }
    }else{ 
        temp=1
    }
    print "return final temp: ${temp}"
    return temp
}

// recibe un map o un JSON(objeto de llave: valor)
def overwriteParams(parametersToInsert) {
    echo "overwriteParams(${parametersToInsert})"
    dir("quarkus-deploy-pipeline") {
        sh "pwd"
        sh "ls"
        def params = readJSON file: './params.tfvars.json'
        def newParams = [:]
        params.each { param -> newParams[param.key] = param.value }
        parametersToInsert.each { param -> newParams[param.key] = param.value }
        def newParamsJson = new JsonOutput().toJson(newParams)
        sh "rm -R params.tfvars.json"
        print newParamsJson
        writeFile file: "params.tfvars.json", text: newParamsJson.toString()
    }
}

/*---------Pruebas WRK----------*/
def pruebaswrk(){
    echo "------------pruebaswrk()------"
    dir ("/var/lib/jenkins/workspace/wrk"){
        sh './wrk -t12 -c400 -d30s https://ojez8ojan2.execute-api.us-west-2.amazonaws.com//hello'
    }
}

def findProjectById(def gitlabURL, def projectId){
    echo "project_data FROM = ${gitlabURL}/api/v4/projects?search=${projectId}"
    try {
            withCredentials([string(credentialsId: 'credenciales-api-git', variable: 'TOKEN_API_GITLAB')]) {
                reponseGit =
                    httpRequest customHeaders: [
                        [name: 'PRIVATE-TOKEN',
                            value: "${TOKEN_API_GITLAB}"
                        ]
                    ],
                    httpMode: 'GET',
                    ignoreSslErrors: true,
                url: "${gitlabURL}/api/v4/projects/${projectId}"
                RESPONSE_GITLAB = readJSON text: reponseGit.content
            }
    }catch (Exception e){
        return error(" x - - - - -> Ocurrio un error durante la busqueda de Project en gitlab. Exception: ${e}")
    }
    def projectData = null
    if (RESPONSE_GITLAB) {
        projectData = RESPONSE_GITLAB
    }
    return projectData
}

void cloneRepositoryById(int project_data) {
    stage("cloneGitlabRepository(${CI_SERVER_URL}, ${projectId})") {
        print "------------------> stage(cloneGitlabRepository(${CI_SERVER_URL}, ${projectId})) <------------------"
        CHECKOUT_VARS = checkout scm
        withCredentials([string(credentialsId:'credenciales-api-git', variable: 'TOKEN_API_GITLAB')]) {
            RESPONSE_GITLAB =
                httpRequest customHeaders: [[name :'PRIVATE-TOKEN',
                                                value: TOKEN_API_GITLAB]],
                        ignoreSslErrors: true,
                        url:"${CI_SERVER_URL}/api/v4/projects/${projectId}"
        }
        PROJECT_NAME = readJSON text:"${RESPONSE_GITLAB.content}"
        PROJECT_NAME = "${PROJECT_NAME.name.trim()}"
        echo "Project Name : ${PROJECT_NAME}"
    }
}

def cloningMicroservice(){
    stage("cloningMicroservice()"){
        datosRepo = findProjectById(CI_SERVER_URL, params.projectId)
        sh "mkdir ${datosRepo.name}"
            dir("${datosRepo.name}"){
                git branch: "${BRANCH_NAME}",
                credentialsId: 'GitlabCredenciales',
                url: "${datosRepo.http_url_to_repo}"
                sh "ls -lat"
            }
        // print "------------------>stage(cloningMicroservice())<------------------"
        // datosRepo = findProjectById(CI_SERVER_URL, params.projectId)
        // sh "git clone --branch ${BRANCH_NAME} ${datosRepo.http_url_to_repo}"
    }
}



def replaceFile(def fileName){
    print "-----> replaceFile(${fileName})" 
    
    // deployInfo.each{indice ->
    //      print "deployInfo."+indice
    // }
    try{
        def fileToReplace = readFile file: "${fileName}"
        //print "fileToReplace: ${fileToReplace}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_JFROG_USER", "${deployInfo.jfrogUser}")
        //print "REPLACE_JFROG_USER: ${deployInfo.jfrogUser}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_JFROG_PASS", "${deployInfo.jfrogPass}")
        //print "REPLACE_JFROG_PASS: ${deployInfo.jfrogPass}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_JFROG_URL", "${deployInfo.jfrogUrl}")
        //print "REPLACE_JFROG_URL: ${deployInfo.jfrogUrl}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_JFROG_CONTEXT_URL", "${deployInfo.jfrogContextUrl}")
        //print "REPLACE_JFROG_CONTEXT_URL: ${deployInfo.jfrogContextUrl}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_JFROG_REPO_KEY", "${deployInfo.jfrogRepoKey}")
        //print "REPLACE_JFROG_REPO_KEY: ${deployInfo.jfrogRepoKey}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_PROYECT_NAME", "${deployInfo.projectName}")        
        //print "REPLACE_PROYECT_NAME:${deployInfo.projectName}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_ECR_NAME", "${deployInfo.ecrName}")        
        //print "REPLACE_ECR_NAME:${deployInfo.ecrName}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_TAG_VERSION", "${deployInfo.tagVersion}")
        //print "REPLACE_TAG_VERSION:${deployInfo.tagVersion}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_SERVICE_NAME", "${deployInfo.serviceName}")
        //print "REPLACE_SERVICE_NAME:${deployInfo.serviceName}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_CLUSTER_NAME", "${deployInfo.cluster}")
        //print "REPLACE_CLUSTER_NAME:${deployInfo.cluster}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_SUBNETS", "${deployInfo.subnets}")
        //print "REPLACE_SUBNETS:${deployInfo.subnets}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_VPC_ID", "${deployInfo.vpc}")
        //print "REPLACE_VPC_ID:${deployInfo.vpc}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_DOCKER_IMAGE_TAG", "${deployInfo.dockerImageTag}")
        //print "REPLACE_DOCKER_IMAGE_TAG:${deployInfo.dockerImageTag}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_TG_NAME", "${deployInfo.targetGroup.toUpperCase()}")
        //print "REPLACE_TG_NAME:${deployInfo.targetGroup}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_LOG_GROUP_CLOUDWATCH_NAME", "${deployInfo.logGroup}")
        //print "REPLACE_LOG_GROUP_CLOUDWATCH_NAME:${deployInfo.logGroup}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_CLOUDWATCH_NAME", "${deployInfo.cloudWatch}")
        //print "REPLACE_CLOUDWATCH_NAME:${deployInfo.cloudWatch}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_TASK_DEFINITION_FAMILY", "${deployInfo.taskDefinition}")
        //print "REPLACE_TASK_DEFINITION_FAMILY:${deployInfo.taskDefinition}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_CONTAINER_NAME", "${deployInfo.container}")
        //print "REPLACE_CONTAINER_NAME:${deployInfo.container}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_SECURITY_GROUP_NAME", "${deployInfo.securityGroup}")
        //print "REPLACE_SECURITY_GROUP_NAME:${deployInfo.securityGroup}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_APLICATION_PORT", "${deployInfo.aplicationPort}")
        //print "REPLACE_APLICATION_PORT:${deployInfo.aplicationPort}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_LISTENER_ARN", "${deployInfo.listenerArn}")
        //print "REPLACE_LISTENER_ARN:${deployInfo.listenerArn}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_LISTENER_PRIORITY", "${deployInfo.listenerPriority}")
        //print "REPLACE_LISTENER_PRIORITY:${deployInfo.listenerPriority}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_BASE_PATH","${deployInfo.basePath}")
        //print "REPLACE_BASE_PATH: ${deployInfo.basePath}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_HEALT_CHECK_PATH","${deployInfo.healthCheckPath}")
        //print "REPLACE_HEALT_CHECK_PATH: ${deployInfo.healthCheckPath}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_REGION","${deployInfo.region}")
        //print "REPLACE_REGION: ${deployInfo.region}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_CPU","${deployInfo.cpu}")
        //print "REPLACE_CPU: ${deployInfo.cpu}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_MEMORY","${deployInfo.memory}")
        //print "REPLACE_MEMORY: ${deployInfo.memory}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_TASK_ROLE","${deployInfo.taskRole}")
        //print "REPLACE_TASK_ROLE: ${deployInfo.taskRole}"
        fileToReplace = fileToReplace.replaceAll("REPLACE_EXECUTION_ROLE","${deployInfo.executionRole}")
        //print "REPLACE_EXECUTION_ROLE: ${deployInfo.executionRole}"
        writeFile file: "${fileName}", text: fileToReplace
        //fileToReplace = readFile file: "${fileName}"
        //print "fileToReplace: ${fileToReplace}"

    }catch(Exception e){
        return error("No se pudo editar el arcihvo ${fileName}, revisa si existe. Exception: ${e}")
    }
}


//**************************** Clean Files ****************************
def cleanTfFiles() {
    print "cleanTfFiles()"
    try {
        sh "rm -R main.tf"
        sh "rm -R .terraform"
        sh "rm -R .terraform.lock.hcl"
        //sh "rm -R terraform.tfstate"
    }catch (error) {
        echo "Ocurrio un error al limpiar archivos de terraform: ${error}" 
    }
}
//**************************** Clean Files ****************************

//*************************** ECR - INICIO ***************************

def validationECR(def data){
    def ecrName = data.ecrName
    print "validationECR(${ecrName})"
    print data.awsProfile
    def awsProfile = data.awsProfile
    print awsProfile 
     // validando si existe un ECR ya creado
    def isExistRepository = false
    try {
        echo "aws ${awsProfile} ecr describe-repositories --repository ${ecrName}"
        sh "aws ${awsProfile} ecr describe-repositories --repository ${ecrName}"
        isExistRepository = true
    }catch(error) {
        echo "El repositorio ${ecrName} no existe, se desplegará"
        isExistRepository = creationECR(ecrName)
    }
    print "validationECR: ${isExistRepository}"
    return isExistRepository
}

def creationECR(def ecrName){
    stage("creationECR(${ecrName})"){
        print "creationECR(${ecrName})"
        dir("quarkus-deploy-pipeline") {
            replaceFile("ecrPrevio.terraform")
            sh "mv ecrPrevio.terraform main.tf"
            try {
                //creando AWS ECR
                sh 'terraform init'
                //sh 'terraform plan'
                sh "terraform apply -no-color -var-file='params.tfvars.json' -auto-approve"
                //cleanTfFiles()
                print "AWS ECR ${ecrName} creado con exito"
            } catch (error) {
                echo "Ocurrio un error al crear AWS ECR ${ecrName}: ${error}" 
            }
        }
        try{
            sh "aws ${awsProfile} ecr describe-repositories --repository ${ecrName}"
            isCreatedRepository = true
        }catch(error){
             echo "El repositorio ${ecrName} no existe"
             isCreatedRepository = false
        }
         print "creationECR(${isCreatedRepository})"
         return isCreatedRepository
    }
}
//*************************** ECR - FIN ***************************
//*************************** Image *******************************
def buildImage(data){
    print "buildImage(${data.ecrArn}/${data.projectName}:version)"
    def serviceName = data.projectName
    def accountId = data.accountId
    def region = data.region
    def ecrArn = data.ecrArn
    stage("buildImage( serviceName: ${serviceName}, accountId: ${accountId}, region: ${region})"){
        dir("${serviceName}") {
            def imageName = "${serviceName}"             
            def tagVersion =  getTagVersion(data)
            deployInfo.tagVersion = tagVersion
            print "dockerImageTag: ${ecrArn}/${serviceName}:${tagVersion}"
            def dockerImageTag = "${ecrArn}/${serviceName}:${tagVersion}"
            deployInfo.dockerImageTag = dockerImageTag
            print "dockerImageTag: "+dockerImageTag
            //replaceFile("build.gradle")
            print "------------- Inicia Construye imagen-----------------------------"
            
                sh """./gradlew build \
                -Dquarkus.package.type=native -x test \
                -Dquarkus.profile=aws \
                -Dquarkus.native.additional-build-args=-H:ReflectionConfigurationFiles=reflexionConfig.json,-H:ReflectionConfigurationFiles=reflexionConfig.json"""
                sh "docker build -f ./src/main/docker/Dockerfile.native-micro -t ${dockerImageTag} ."
                sh "docker images"

            print "------------- Termina Construye imagen-----------------------------"
            //sh "docker images"
        }
    }
}

def pushImageECR(def data){
    stage("pushImageECR: ${data.dockerImageTag}"){                
        print "pushImageECR: ${data.dockerImageTag}"
        def awsProfile = data.awsProfile
        def dockerImageTag = data.dockerImageTag
        def ecrArn = data.ecrArn                
        def region = data.region                                            
        sh "aws ${awsProfile} ecr get-login-password --region ${region} | docker login --username AWS --password-stdin ${ecrArn}"        
        def isPushedImage = false;
        print isPushedImage
        //correosLogs()
        def rootDirectory = pwd()
        echo "rootDirectory: ${rootDirectory}"
        try {
            sh "docker push ${dockerImageTag}"
            isPushedImage = true
            echo "La imagen fue desplegada en ECR de manera exitosa"
        } catch(error) {
            echo "Error: La imagen no se pudo subir a ECR: ${error}"
            isPushedImage = false
        }
        return isPushedImage
    }
}

def getTagVersion(def data){
    def serviceName = data.projectName
    print "getTagVersion"
    // obtener la tag para la versión de la imagen
    def awsProfile = data.awsProfile
    try {
        def version = sh (script:"aws ${awsProfile} ecr describe-images --repository-name ${serviceName} --query 'sort_by(imageDetails,& imagePushedAt)[-1].imageTags[*]' --output yaml | sort -r | grep -Eo '[0-9].[0-9].[0-99]' | head -1", returnStdout: true)
        version=version.replaceAll("\n","")
        print "version: ${version}"
        if(version.length() > 1){
        echo "if(${version.length()} > 1)"
        version = generateNewTagVersion(version)
        }    
        else{
        version = "0.0.1"
        }
        println "----> Nueva Ultima Version en ECR Registry :'${version}'"
        deployInfo.latestVersion = version
        print "version: ${version}"
        println "----> Nueva Ultima Version en ECR Registry :'${version}'"
        return version
    }catch(error) {
        echo "No se pudo obtener la versión de la imagen por lo tanto será latest: ${error}" 
    }
}

def generateNewTagVersion(def version) {
    print "generateTag(${version})"
    def ultimaVersion = version
    ultimaVersion = ultimaVersion.trim().split("[.]") 
    //print "ultimaVersion = ${ultimaVersion}"
    //print "ultimaVersion[0].toInteger()= ${ultimaVersion[0].toInteger()}"
    //print "ultimaVersion[1].toInteger()= ${ultimaVersion[1].toInteger()}"
    //print "ultimaVersion[2].toInteger()= ${ultimaVersion[2].toInteger()}"
    
    if(ultimaVersion[2].toInteger()<9){ 
        ultimaVersion[2] =+ ultimaVersion[2].toInteger()+1 
    }
    if(ultimaVersion[2].toInteger()==9){ 
        ultimaVersion[1] =+ ultimaVersion[1].toInteger()+1 
        ultimaVersion[2] = "0" 
    }
    ultimaVersion = "${ultimaVersion[0]}.${ultimaVersion[1]}.${ultimaVersion[2]}"   
    //echo "ultimaVersion = ${ultimaVersion[0]}.${ultimaVersion[1]}.${ultimaVersion[2]}"
    print "ultimaVersion = ${ultimaVersion}"
    return ultimaVersion
    //return "latest"
}



//*************************** Image *******************************

//*************************** Inicio Servicio *******************************
def isExistService(def data){
    def serviceName = data.serviceName
    def cluster = data.cluster
    // validando si existe un servicio ya creado
    print 'validando si existe un servicio ya creado'
    def awsProfile = data.awsProfile
    def existService = false
    try {
        def servicesRequest = sh (script: "aws ${awsProfile} ecs list-services --cluster ${cluster}", returnStdout: true)
        print "servicesRequest: ${servicesRequest}"
        def services = jsonParse(servicesRequest)
        def service = services.serviceArns.find { thisService -> thisService.contains(serviceName) }
        print "service: ${service}"
        if(service) {
            existService = true 
        }
    }catch(error) {
        echo "Error al validar la existencia de componentes: ${error}" 
    }
    print "existService: ${existService}"
    return existService
}
//*************************** Fin Servicio *******************************

 //*************************** ECS CREATE ALL - INICIO ***************************
   
    
   
def createAllECS(){
    // si no existe desplegar TD, TG y SERVICE
    stage("createAllECS"){
        print "stage(createAllECS)"
        //dir("quarkus-deploy-pipeline"){
            replaceFile("ecs.terraform")
            print 'si no existe desplegar Task Definition, Target Group y SERVICE'
            def isCreatedComponents = false
            // haciendo main el tf del ECS a desplegar
            print 'haciendo main el tf del ECS a desplegar'       
            sh "mv ecs.terraform main.tf"
            try {
                sh 'terraform init'
                //sh "terraform plan"
                sh "terraform apply -no-color -var-file='params.tfvars.json' -auto-approve"
                isCreatedComponents = true
                print "isCreatedComponents: ${isCreatedComponents}"
                cleanTfFiles()
                echo "Componentes aws creados con éxito"
            } catch (error) {
                echo "Ocurrio un error al crear componentes aws: ${error}" 
            }
            // clausula de guarda para salir si no pudo crear componentes aws TD o TG o SERVICE
            if(!isCreatedComponents) {
                echo 'clausula de guarda para salir si no pudo crear componentes aws TD o TG o SERVICE'
                return 
            }
        //}
    }
}
    
//*************************** ECS CREATE ALL  - FIN ***************************


//*************************** ECS CREATE NEW TASK REVISION - INICIO ***************************
    def newTaskRevision(){
        stage("new Task Revisión"){
            echo 'new Task Revisión'
            def isCreatedNewTDRevision = true
            // haciendo main el tf del ECS a actualizar
            echo 'haciendo main el tf del ECS a actualizar'
            replaceFile("ecs_new_td_revision.terraform")
            sh "mv ecs_new_td_revision.terraform main.tf"
            sh "terraform init"
            sh "terraform plan"
            sh "terraform apply -no-color -var-file='params.tfvars.json' -auto-approve"
            //*************************** ECS UPDATE SERVICE - INICIO ***************************            
        }
    }

    def updateService(def data){
        def isUpdatedService = true
        def cluster = data.cluster
        def serviceName = data.serviceName
        def taskDefinition = data.taskDefinition
        def awsProfile = data.awsProfile
        try {
            // creando componentes aws TD, TG y SERVICE
            echo 'creando componentes aws TD, TG y SERVICE'
            sh "aws ${awsProfile} ecs update-service --cluster ${cluster} --service ${serviceName} --task-definition ${taskDefinition} --force-new-deployment"
        } catch (error) {
            isUpdatedService = false
            echo "Ocurrio un error al actualizar ECS Service: ${error}" 
        }
        if(!isUpdatedService) {
            echo "Error al actualizar aws service"

            return
        }     

    }

    def purgaImagenes(def data){
        def dockerImageTag = data.dockerImageTag
        stage("Docker Prune Images"){
            sh "docker image rmi ${dockerImageTag}"
            deleteDir()
        }
    }//----------->

def sendNotifications(def String statusPipeline){
    def date = LocalDateTime.now()
    def serviceName =  "${deployInfo.projectName}"
    def telegramGroupId = "-873535927"
    def BUILD_TRIGGER_BY = "${currentBuild.getBuildCauses()[0].shortDescription} / ${currentBuild.getBuildCauses()[0].userId}"
    /* Mensaje PROD */
    def mensajeProd = """ \"text\":\"*DEPLOY ${statusPipeline} ${serviceName}*
    - *Date Deploy* : _${date}_
    - *Branch deploy* : _${BRANCH_NAME}_
    - *Image* : _${deployInfo.dockerImageTag}_
    - *Ejecution* : _${BUILD_ID}_
    - *BUILD_URL: ${BUILD_URL}
    - _${BUILD_TRIGGER_BY}_\" 
    ***************************************
    """
    textResponse = mensajeProd
    sh "curl -k -X GET https://api.telegram.org/bot5339609629:AAGTbvr-Q61cLu7-4TZLkVRbi9_XrAqeO_g/sendMessage -H 'Content-Type: application/json' -d '{\"chat_id\":${telegramGroupId},${textResponse},\"parse_mode\":\"Markdown\"} '"    
}


return this
